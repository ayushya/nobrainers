<!doctype html>
<html>
<head>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NobRainers Video chat </title>
 
  <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/peer.js"></script>
   <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <script>
  $(document).ready(function(e) {
    var currheight = $(window).height();
	$(".chatlist").css('height',currheight);
	
});
  </script>



  <script>
$(document).ready(function() {
    

    // Compatibility shim
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
	 // PeerJS object
	// var us=prompt('Username');
	
	var us="<?php echo $_POST['username']; ?>";
	 <?php
$myfile = fopen("userlist.txt", "a+") or die("Unable to open file!");
$strval= $_POST['username'] . PHP_EOL;
fwrite($myfile,$strval);

fclose($myfile);
?>





    // No API key required when not using cloud server
    var peer = new Peer(us, {host: '/', port: 9000,path:''});


   
   

    peer.on('open', function(){
      $('#my-id').text(peer.id);
    });

    // Receiving a call
    peer.on('call', function(call){
      // Answer the call automatically (instead of prompting user) for demo purposes
      call.answer(window.localStream);
      step3(call);
    });
    peer.on('error', function(err){
      alert(err.message);
      // Return to step 2 if error occurs
      step2();
    });

    // Click handlers setup
    $(function(){
      $('#make-call').click(function(){
        // Initiate a call!
        var call = peer.call($('#callto-id').val(), window.localStream);

        step3(call);
      });

      $('#end-call').click(function(){
        window.existingCall.close();
        step2();
      });

      // Retry if getUserMedia fails
      $('#step1-retry').click(function(){
        $('#step1-error').hide();
        step1();
      });

      // Get things started
      step1();
    });

    function step1 () {
      // Get audio/video stream
      navigator.getUserMedia({audio: true, video: true}, function(stream){
        // Set your video displays
        $('#my-video').prop('src', URL.createObjectURL(stream));

        window.localStream = stream;
        step2();
      }, function(){ $('#step1-error').show(); });
    }

    function step2 () {
      $('#step1, #step3').hide();
      $('#step2').show();
    }

    function step3 (call) {
      // Hang up on an existing call if present
      if (window.existingCall) {
        window.existingCall.close();
      }

      // Wait for stream on the call, then set peer video display
      call.on('stream', function(stream){
        $('#their-video').prop('src', URL.createObjectURL(stream));
      });

      // UI stuff
      window.existingCall = call;
      $('#their-id').text(call.peer);
      call.on('close', step2);
      $('#step1, #step2').hide();
      $('#step3').show();
    }




});
  </script>


</head>

<body>
<?php
$myfile = fopen("userlist.txt", "a+");
// Output one line until end-of-file
$fdata=fgets($myfile);
while(!feof($myfile)) {
//  echo fgets($myfile) . "<br>";

//echo $fdata;
$fdata = preg_replace("/\r\n/", "", $fdata);
$uarray[]=$fdata;
$fdata=fgets($myfile);
}
$utotal=count($uarray); //this gives 1 more  /// FIXED
//$utotal=$utotal-1;   // no need for this as FIXED file handling
//echo "$utotal"; // for giving total no. of users
fclose($myfile);
?>
<script type="text/javascript">
$(document).ready(function() {
    

var ustot1= "<?php echo $utotal; ?>";
//prompt(ustot1); // This works
var allusers =<?php echo json_encode($uarray); ?>;
for(i=0;i<ustot1;i++)
{ 
	$( ".chatlist" ).append( "<div class='uli'>" + allusers[i] + "</div>" );
}

$(".uli").click(function(){
		var ch=$(this).text();
		$("#callto-id").val(ch);
		$("#make-call").click();
	//	alert(ch);
});

});
</script>



<div class="row start">



  <div class="pure-g">
  <div class="col-md-10 videochat">

      <!-- Video area -->
      <div class="pure-u-2-3" id="video-container">
        <video id="their-video" autoplay></video>
        <video id="my-video" muted autoplay></video>
      </div>

      <!-- Steps -->
      <div class="pure-u-1-3">
        <h2>NobRainers Video Chat</h2>

        <!-- Get local audio/video stream -->
        <div id="step1">
          <p>Please click `allow` on the top of the screen so we can access your webcam and microphone for calls.</p>
          <div id="step1-error">
            <p>Failed to access the webcam and microphone.Please enable the camera and microphone access and Click allow when asked for permission by the browser.</p>
            <a href="#" class="pure-button pure-button-error" id="step1-retry">Try again</a>
          </div>
        </div>

        <!-- Make calls to others -->
        <div id="step2">
          <p>Your id: <span id="my-id">...</span></p>
          <p>Share this id with others so they can call you.</p>
          <h3>Make a call</h3>
          <div class="pure-form">
            <input type="text" placeholder="Call user id..." id="callto-id">
            <a href="#" class="pure-button pure-button-success" id="make-call">Call</a>
          </div>
        </div>

        <!-- Call in progress -->
        <div id="step3" >
          <p>Currently in call with <span id="their-id">...</span></p>
          <p><a href="#" class="pure-button pure-button-error" id="end-call">End call</a></p>
        </div>
      </div>
      </div>
      
      
      
  <!-- Here is the chatlist div -->    
      <div class="col-md-2 chatlist">
</div>



  </div>

</div>



</body>
</html>

